from flask import Flask, request, render_template, jsonify
from pathlib import Path
import json
import re

app = Flask(__name__)
SITE_URI_PREFIX = '/branch_compare'
ARCHS = ('x86_64', 'aarch64')
BRANCHES =  ('stable', 'testing', 'unstable')
DATABASE = {}
LAST_UPDATE = {}
BASE_DIR = Path(__file__).parent
DATA_DIR = BASE_DIR / 'data'
for arch in ARCHS:
    with open(DATA_DIR / f'{arch}.json', 'r') as db_file:
        database_file = json.load(db_file)
    LAST_UPDATE[arch] = database_file['last_update']
    DATABASE[arch] = database_file['packages']

def regex(pattern: str, string: str) -> bool:
    '''Helper function that checks if a string matches a regex pattern.
    This function also catches the case where search function can return an error for some reason.'''
    try:
        return True if re.search(pattern, string) else False
    except:
        return False

@app.route(SITE_URI_PREFIX, methods=['GET'])
def branch_compare():
    '''Main service function.'''
    # Get inputs
    is_arm = request.args.get('arm', '').lower() in ('1', 'on', 'true')
    arch = ARCHS[int(is_arm)]
    query = request.args.get('q', '').lower()
    if query in ('#error', '#eol', '#new', '#manjaro', '#kernels'):
        flag = query.removeprefix('#')
    else:
        flag = ''
    json = request.args.get('json', False)

    # Filter packages based on the query
    packages = {}
    if flag:
        for package_name, package in DATABASE[arch].items():
            if package.get(flag, False):
                packages[package_name] = package
    elif query:
        for package_name, package in DATABASE[arch].items():
            if regex(query, package_name):
                packages[package_name] = package

    # Return the results
    # If JSON is activated, return a JSON object
    if json:
        return jsonify(packages)
    else:
        # If not, render a web page to show results.
        return render_template('branch_compare.html', 
            packages = packages, 
            query = query, 
            arm = is_arm,
            branches = BRANCHES,
            last_update = LAST_UPDATE[arch],
            site_uri_prefix = SITE_URI_PREFIX,
        )
