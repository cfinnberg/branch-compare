#!/usr/bin/env python

from dateutil.parser import parse as parsedate
from dateutil import tz
from datetime import datetime, date
from pathlib import Path
import requests
import tarfile
import json
import subprocess
import tempfile
import re
import os
import logging


# Program constant parameters and logging initialization
logger = logging.getLogger(__name__)
LOGGING_LEVEL = logging.INFO
BASE_DIR = Path(__file__).parent
LOGS_DIR = BASE_DIR.parent / 'logs'
CACHE_DIR = BASE_DIR / 'cache'
DATA_DIR = BASE_DIR / 'data'
LOG_FILE = LOGS_DIR / f'update_db_{date.today().isoformat()}.log'
logging.basicConfig(filename=LOG_FILE, format='%(asctime)s %(levelname)s:%(message)s', encoding='utf-8', level=LOGGING_LEVEL)
logging.info('')
logging.info('*************')
logging.info('Update starts')
logging.info('*************')
MIRROR = 'https://repo.manjaro.org/repo'
ARCHS = ['x86_64', 'aarch64']
BRANCHES = ('stable', 'testing', 'unstable')
MIRROR_BRANCHES = {
    'x86_64': BRANCHES,
    'aarch64': tuple(f'arm-{branch}' for branch in BRANCHES),
}
REPOS = {
    'x86_64': ('core', 'extra', 'multilib'),
    'aarch64': ('core', 'extra', 'mobile', 'kde-unstable'),
}


def read_packages(packages: dict, repo: str, branch: str, arch: str) -> None:
    '''Read a repo's database file (tar.gz) and create a list of packages in-memory.
    The dictionary in the 'packages' parameter get filled with the packages. The functions does not return anything explicitly'''
    tarfilename = CACHE_DIR / f'{arch}-{branch}-{repo}.db.tar.gz'
    actual_branch = branch.removeprefix('arm-')
    with tempfile.TemporaryDirectory(prefix='pacmandb') as tmp_dir:
        tmp_path = Path(tmp_dir)
        with tarfile.open(tarfilename, 'r:gz') as f:
            f.extractall(path=tmp_path, filter='tar')

        for dir in tmp_path.glob('*'):
            *package, version, subversion = dir.name.split('-')
            package_name = '-'.join(package)
            version = f'{version}-{subversion}'
            if not packages.get(package_name, None):
                packages[package_name] = {}
                for branch_name in BRANCHES:
                    packages[package_name][branch_name] = { 'version': '' } 
            packages[package_name][actual_branch]['version'] = version
            packages[package_name][actual_branch]['repo'] = repo

            # Is this a Manjaro package? Find out checking the PACKAGER
            with open(dir / 'desc','r') as file:
                descriptor = file.read().strip().split('\n\n')
            for field in descriptor:
                property, *value = field.split('\n')
                property = property.strip('%')
                if property != 'PACKAGER':
                    continue
                packages[package_name][actual_branch]['Manjaro'] = bool(value[0].lower().count('manjaro'))
                break

def sync_remote(branches: list, repos: list, arch:str) -> bool:
    '''Retrieve packages' databases for the given branches and repos from a mirror (defined globally).'''
    changes = False

    # Iterate branches
    for branch in branches:
        # If branch does not yet have a folder, create it
        branch_cache = CACHE_DIR / branch
        if not branch_cache.exists():
            logger.info(f'Creating {branch_cache}')
            branch_cache.mkdir()
        
        # Iterate repos for this branch
        for repo in repos:
            remote_filename = f'{repo}.db.tar.gz'
            
            # Get remote database file's datetime
            remote_url = f'{MIRROR}/{branch}/{repo}/{arch}/{remote_filename}'
            logger.info('')
            logger.info(f'Remote head {branch}:{repo}')
            r = requests.head(url=remote_url, timeout=30)
            if not r.ok:
                logger.warning(f"Can't get remote head for {remote_url}. Skipping repo {branch}:{repo}")
                continue
            remote_datetime = parsedate(r.headers['Last-Modified']) # With .astimezone() is local time
            logger.info(f'Remote file\'s datetime {remote_datetime}')
            
            # Get local file's datetime
            localfilename = branch_cache / f'{repo}_{remote_datetime.strftime('%Y%m%d_%H%M%S')}.db.tar.gz'
            if localfilename.exists():
                logger.info('Local file is up to date. Skipping.')
                continue
            
            # Download the database file for this repo and branch
            logger.info(f'Corresponding local file not present. Downloading remote file {remote_filename}')
            r = requests.get(url=remote_url, timeout=30)
            if r.ok:
                changes = True
                logger.info(f'Writing downloaded file to local file {localfilename}')
                with open(localfilename, 'wb') as f:
                    f.write(r.content)
                os.utime(localfilename, (datetime.now().timestamp(), remote_datetime.astimezone(tz.tzutc()).timestamp()))
            else:
                logger.warning(f'Download failed for {remote_url}. Skipping this file for now...')
                continue

            # Link
            db_link = CACHE_DIR / f'{arch}-{branch}-{repo}.db.tar.gz'
            if db_link.exists():
                db_link.unlink()
            db_link.symlink_to(localfilename.relative_to(db_link.parent))
    return changes

def create_package_list(branches: list, repos: list, arch: str) -> dict:
    '''Calls the function to create in-memory packages' data from the given branches and repos.'''
    logger.info('')
    logger.info('Composing new package list...')
    packages = {}
    for branch_name in branches:
        for repo_name in repos:
            read_packages(packages, repo=repo_name, branch=branch_name, arch=arch)

    for package_name, package in packages.items():
        s = bool(package['stable']['version'])
        t = bool(package['testing']['version'])
        u = bool(package['unstable']['version'])

        if (not s and t and not u) or (s and not t and u):
            packages[package_name]['error'] = True

        if s and not u:
            packages[package_name]['eol'] = True

        if not s and u:
            packages[package_name]['new'] = True

        for branch_name in branches:
            branch_name = branch_name.removeprefix('arm-')
            if package[branch_name].get('Manjaro', False):
                packages[package_name]['manjaro'] = True
                break
        
        pattern = r'^linux\d+$' if arch == 'x86_64' else r'^linux-[^-]+$'
        if re.search(pattern, package_name):
            packages[package_name]['kernels'] = True

    return {name: packages[name] for name in sorted(packages)}

def get_last_update_datetime(arch: str) -> str:
    '''Returns the datetime of the last update of the given architecture'''
    dt = [ '_'.join(file.readlink().name.split('.')[0].split('_')[1:]) for file in CACHE_DIR.glob(f'{arch}-*') ]
    return sorted(dt)[-1]

def save_to_database(packages: dict, arch: str) -> None:
    '''Saves in-memory packages' data to database on disk'''
    last_update_datetime = get_last_update_datetime(arch)
    if not DATA_DIR.exists():
        logger.info(f'Creating Data Directory "{DATA_DIR}"')
        DATA_DIR.mkdir()
    db = DATA_DIR / f'{arch}_{last_update_datetime}.json'

    if db.exists():
        logger.info('Database file already exists. Discarding data in memory.')
    else:
        logger.info(f'Saving data to database {db}')
        lud = last_update_datetime
        last_update = f'{lud[:4]}-{lud[4:6]}-{lud[6:8]} {lud[9:11]}:{lud[11:13]}:{lud[13:]} (UTC)'
        database = {
            'packages': packages,
            'last_update': last_update,
        }
        with open(db, 'w') as db_file:
            json.dump(database, db_file)

    db_link = DATA_DIR / f'{arch}.json'
    if db_link.exists():
        db_link.unlink()
    db_link.symlink_to(db.relative_to(db_link.parent))

    return

if __name__ == '__main__':
    if not CACHE_DIR.exists():
        CACHE_DIR.mkdir()
    
    reload = False
    for arch in ARCHS:
        if sync_remote(MIRROR_BRANCHES[arch], REPOS[arch], arch):
            db = DATA_DIR / f'{arch}_{get_last_update_datetime(arch)}.json'
            if db.exists():
                logger.info('Database file already exists for this update. Skipping database recreation.')
            else:
                reload = True
                packages = create_package_list(MIRROR_BRANCHES[arch], REPOS[arch], arch)
                save_to_database(packages, arch)

    if not Path('.dev').exists():   # Skip daemon reload on development environment
        if reload:
            argv = ['systemctl', 'reload', 'gunicorn_branch']
            subprocess.run(argv)

    logging.info('')
    logging.info('**********')
    logger.info('Update ends')
    logging.info('**********')
    logging.info('')
