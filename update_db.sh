#!/bin/fish

set Script (path resolve (status --current-filename))           # Current script full path
set ScriptFullName (path basename $Script)                      # Script's filename (with extension)
set ScriptName (path change-extension '' $ScriptFullName)       # Script's filename without extension
set ScriptRoot (path dirname $Script)                           # Script's base folder / root folder
set ScriptBaseName (path basename $ScriptRoot)                  # Script's folder name

set AppName $ScriptBaseName


cd $ScriptRoot/$AppName

$ScriptRoot/venv/bin/python $ScriptName.py >> $ScriptRoot/logs/$ScriptName.log 2>&1

# Remove old log files (older than 30 days)
for file in $ScriptRoot/logs/$ScriptName_*.log
	if test (path basename $file | path change-extension '' | cut -d'_' -f 3) -lt (date '+%Y-%m-%d' -d '-30 days')
		rm $file
	end
end
